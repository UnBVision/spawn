import argparse
from utils import path_config


# default settings
GPU = 0

# Dataloader settings
WORKERS = 2
DATASET = "NYU"
PREPROC_PATH = ""
BASE_PATH_TRAIN = ""
BASE_PATH_TEST = ""

# Model settings
WEIGHTS = "none"
BATCH_NORM = True
INSTANCE_NORM = False
INPUT_TYPE = "rgb+normals"
FINE_TUNE = False

vox_shape = (240, 144, 240)
vox_shape_down = (60, 36, 60)
batch_shape = (1, 60, 36, 60)
batch_shape_ch = (1, 60, 36, 60, 12)

nyu_classes = ["ceil", "floor", "wall", "window", "chair", "bed", "sofa", "table", "tvs", "furniture", "objects",
               "empty"]


def parse_arguments():
    global GPU, \
           WORKERS, DATASET, BASE_PATH_TRAIN, BASE_PATH_TEST, PREPROC_PATH, \
           WEIGHTS, BATCH_NORM, INSTANCE_NORM, INPUT_TYPE, FINE_TUNE,SCENE

    print("\nMMNet Preprocessing Script\n")

    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", help="Target dataset", type=str, choices=['SUNCG', 'NYU', 'NYUCAD'])
    parser.add_argument("weights", help="Pretraind 2D weights to preprocess.", type=str)
    parser.add_argument("scene", help="Scene to preprocess.", type=str)
    parser.add_argument("--workers", help="Concurrent threads. Default " + str(WORKERS),
                        type=int, default=WORKERS, required=False)
    parser.add_argument("--gpu", help="GPU device. Default " + str(GPU),
                        type=int, default=GPU, required=False)
    parser.add_argument("--input_type",  help="Network input type. Default " + INPUT_TYPE,
                        type=str, default=INPUT_TYPE, required=False,
                        choices=['rgb+normals', 'rgb']
                        )
    parser.add_argument("--fine_tune",  help="Is it a fine tune from a suncg model?. Default no",
                        type=str, default="no", required=False,
                        choices=['yes', 'Yes', 'y', 'Y', 'no', 'No', 'n', 'N']
                        )

    args = parser.parse_args()

    DATASET = args.dataset
    SCENE = args.dataset
    WORKERS = args.workers
    GPU = args.gpu
    WEIGHTS = args.weights
    INPUT_TYPE = args.input_type
    FINE_TUNE = args.fine_tune in ['yes', 'Yes', 'y', 'Y']

    path_dict = path_config.read_config()

    if DATASET == "NYU":
        BASE_PATH_TRAIN = path_dict["NYU_BASE_TRAIN"]
        BASE_PATH_TEST = path_dict["NYU_BASE_TEST"]
        if not FINE_TUNE:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["NYU_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYU_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["NYU_RGB_PRIOR_FINE_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYU_RGB_NORMALS_FINE_PRIOR_PREPROC"]

    elif DATASET == "SUNCG":
        BASE_PATH_TRAIN = path_dict["SUNCG_BASE_TRAIN"]
        BASE_PATH_TEST = path_dict["SUNCG_BASE_TEST"]
        if not FINE_TUNE:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["SUNCG_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["SUNCG_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            print("Fine-tuning from SUNCG is only possible with NYU or NYUCAD Datasets")
            exit(-1)
    else:
        print("Dataset", DATASET, "not supported yet!")
        exit(-1)


def preproc():

    from cuda.preproc3d import lib_preproc_setup, process
    from utils.data import get_file_prefixes_from_path
    import numpy as np

    from tqdm import tqdm
    import os
    from torch.utils.data import DataLoader
    from utils.data import MultimodalDataset
    from utils.transforms2D import RandomResize, RandomCrop, CenterCrop, HorizontalFlip, Resize, ToTensor, Normalize, Pad
    from utils.data import get_file_prefixes_from_path
    from torchvision.transforms import Compose
    from models.lw_rdfnet import get_bimodal_RDFNet
    from models.refinenet import get_RefineNet
    import torch
    import torch.nn.functional as F
    from utils.cuda import get_device
    import numpy as np
    from utils.data import DL2Dev
    from tqdm import tqdm
    from skimage import io
    from utils.image import decode_outputs

    with open("bad_bin_files.txt","r") as f:
        bad_bin_files = f.readlines()
    bad_bin_files = [x[:-1] for x in bad_bin_files]

    print("Bad files so far:", len(bad_bin_files))

    with open("zero_bin_files.txt","r") as f:
        zero_bin_files = f.readlines()
    zero_bin_files = [x[:-1] for x in zero_bin_files]

    print("Zero files so far:", len(zero_bin_files))


    print("Selected device:", "cuda:" + str(GPU))

    dev = get_device("cuda:" + str(GPU))
    torch.cuda.empty_cache()

    base_path = {
            'train': BASE_PATH_TRAIN,
            'valid': BASE_PATH_TEST
    }

    #prefixes = {
    #     'train': [#"/d02/data/suncg_out/train/08/0814b0aaa416eeb1394735d74d4b7a36_fl002_rm0002_000003",
    #               #"/d02/data/suncg_out/train/08/0814b0aaa416eeb1394735d74d4b7a36_fl002_rm0002_000004",
    #               #"/d02/data/suncg_out/train/0e/0ea5eadf991d8ed02ddf5a5c007f1a01_fl001_rm0008_000017",
    #               "/d02/data/suncg_out/train/0e/0ea5eadf991d8ed02ddf5a5c007f1a01_fl001_rm0008_000018",
    #               #"/d02/data/suncg_out/train/12/12d1e69af34ba4eb1a943506a150eb3c_fl001_rm0001_000001"
    #               ]
    #}
    prefixes = [SCENE]

    transforms = Compose([CenterCrop((468, 625)),
                          ToTensor(),
                          Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
                          ])


    if INPUT_TYPE == 'rgb+normals':
        train_ds = MultimodalDataset(prefixes, transf=transforms, read_normals=True, read_xyz=False)
        model, encoder_parameters, decoder_parameters = get_bimodal_RDFNet(num_classes=11)
    else:
        train_ds = MultimodalDataset(prefixes, transf=transforms, read_normals=False, read_xyz=False)
        model, encoder_parameters, decoder_parameters = get_RefineNet(num_classes=11)

    dataloader = DL2Dev(DataLoader(train_ds, batch_size=1, shuffle=False, num_workers=WORKERS), dev),

    model.load_state_dict(torch.load(os.path.join("weights", WEIGHTS)))

    for param in model.parameters():
        param.requires_grad = False

    model = model.to(dev)
    model = model.eval()

    v_unit = 0.02

    floor_high = 0.0 if DATASET == "SUNCG" else 4.0

    lib_preproc_setup(device=GPU, num_threads=128, K=None, frame_shape=(640, 480), v_unit=v_unit,
                      v_margin=0.24, floor_high=floor_high, debug=0)

    with tqdm(total=len(prefixes), desc="") as pbar:

        for prefix, (inputs, labels) in zip(prefixes, dataloader):

            basename = os.path.basename(prefix)

            pbar.set_description(prefix)
            pbar.update()

            pred = model(inputs[0], inputs[1]) if INPUT_TYPE == 'rgb+normals' else model(inputs[0])

            outputs = F.interpolate(pred, size=labels.shape[1:], mode="bilinear",
                                    align_corners=False)

            outputs = torch.nn.Softmax(dim=1)(outputs)

            pred_rgb = io.imread(prefix + "_color.jpg")

            out_h, out_w = pred_rgb.shape[:-1]
            in_h, in_w = labels.shape[1:]

            top, left = (out_h - in_h) // 2, (out_w - in_w) // 2

            pred_rgb[top:top + in_h, left:left + in_w:] = decode_outputs(inputs[0], outputs, labels)[0]

            pred_data = outputs.transpose(1, 3)  # NCHW => NWHC
            pred_data = pred_data.transpose(1, 2).cpu().detach().numpy()[0]  # NWHC => NHWC

            pred_out = np.zeros((480, 640, 11), np.float32)
            pred_out[top:top + in_h, left:left + in_w:] = pred_data

            vox_grid, vox_tsdf, vox_prior, segmentation_label, vox_weights = process(prefix, pred_out, vox_shape)


# Main Function
def main():
    parse_arguments()
    preproc()


if __name__ == '__main__':
  main()


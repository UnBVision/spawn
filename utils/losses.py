import torch.nn as nn
from torch import Tensor
import numpy as np
import torch


def get_class_weights(dataloader, num_classes, dev):
    # get class frequencies
    freq = np.zeros((num_classes,))
    for _, labels in dataloader:
        for c in range(num_classes):
            freq[c] += torch.sum(labels == c)
    class_probs = freq / np.sum(freq)

    class_weights = np.append((1/class_probs)/np.sum(1/class_probs),0)

    print(class_weights)
    print(sum(class_weights))
    return torch.from_numpy(class_weights.astype(np.float32)).to(dev)


class CustomCrossEntropy(nn.CrossEntropyLoss):
    __constants__ = ['ignore_index', 'reduction']
    ignore_index: int

    def __init__(self, weight: Tensor = None, size_average=None, ignore_index: int = -100,
                 reduce=None, reduction: str = 'mean', num_classes=40) -> None:
        super(CustomCrossEntropy, self).__init__(weight, size_average, ignore_index, reduce, reduction)
        self.num_classes = num_classes

    def forward(self, input: Tensor, target: Tensor) -> Tensor:
        #print(input.shape)
        #print(target.shape)

        input = input.view(input.size(0), input.size(1), -1)  # N,C,H,W => N,C,H*W
        input = input.transpose(1, 2)  # N,C,H*W => N,H*W,C

        target = target.view(target.size(0), -1)  # N,H,W => N,H*W

        #print(input.shape)
        #print(target.shape)

        idx = target < self.num_classes

        input = input[idx]
        target = target[idx]

        return super(CustomCrossEntropy, self).forward(input, target)


class SSCCrossEntropy(nn.CrossEntropyLoss):
    __constants__ = ['ignore_index', 'reduction']
    ignore_index: int

    def __init__(self, weight: Tensor = None, size_average=None, ignore_index: int = -100,
                 reduce=None, reduction: str = 'mean', num_classes=12) -> None:
        super(SSCCrossEntropy, self).__init__(weight, size_average, ignore_index, reduce, reduction)
        self.num_classes = num_classes

    def forward(self, input: Tensor, target: Tensor, weights: Tensor) -> Tensor:
        #print(input.shape)
        #print(target.shape)

        input = input.view(input.size(0), input.size(1), -1)  # N,C,H,W => N,C,H*W
        input = input.transpose(1, 2)  # N,C,H*W => N,H*W,C

        target = target.view(target.size(0), -1)  # N,H,W => N,H*W
        weights = weights.view(weights.size(0), -1)  # N,H,W => N,H*W

        #print(input.shape)
        #print(target.shape)

        idx = weights != 0.0

        input = input[idx]
        target = target[idx]

        return super(SSCCrossEntropy, self).forward(input, target.type(torch.LongTensor).to(weights.device))


class WeightedSSCCrossEntropy(nn.CrossEntropyLoss):
    __constants__ = ['ignore_index', 'reduction']
    ignore_index: int

    def __init__(self, weight: Tensor = None, size_average=None, ignore_index: int = -100,
                 reduce=None, reduction: str = 'mean', num_classes=12) -> None:
        super(WeightedSSCCrossEntropy, self).__init__(weight, size_average, ignore_index, reduce, reduction)
        self.num_classes = num_classes

    def forward(self, input: Tensor, target: Tensor, weights: Tensor) -> Tensor:
        #print(input.shape)
        #print(target.shape)

        input = input.view(input.size(0), input.size(1), -1)  # N,C,H,W => N,C,H*W
        input = input.transpose(1, 2)  # N,C,H*W => N,H*W,C

        target = target.view(target.size(0), -1)  # N,H,W => N,H*W
        weights = weights.view(weights.size(0), -1)  # N,H,W => N,H*W

        #print(input.shape)
        #print(target.shape)

        epsilon = 1e-10

        occluded = torch.logical_and(~torch.abs(weights).eq(1.0), ~weights.eq(0))

        occupied = torch.abs(weights).eq(1.0)

        #ratio = (4. * torch.sum(occupied)) / (torch.sum(occluded) + epsilon)
        ratio = (2. * torch.sum(occupied)) / (torch.sum(occluded) + epsilon)
        #ratio = (1. * torch.sum(occupied)) / (torch.sum(occluded) + epsilon)

        rand = torch.rand(size=list(target.size())).to(weights.device)

        idx = torch.logical_or(occupied, torch.logical_and(occluded, rand <= ratio))
        #idx = ~weights.eq(0)

        input = input[idx]
        target = target[idx]

        return super(WeightedSSCCrossEntropy, self).forward(input, target.type(torch.LongTensor).to(weights.device))



def weighted_categorical_crossentropy(y_pred, y_true, weights):

    epsilon = 1e-10

    occluded = torch.logical_and(~torch.abs(weights).eq(1.0), ~weights.eq(0))

    occupied = torch.abs(weights).eq(1.0)

    ratio = (2. * torch.sum(occupied))/ (torch.sum(occluded) + epsilon)

    rand = torch.rand(size=list(y_true.size())).to(weights.device)

    print(ratio)

    w = torch.logical_or(occupied, torch.logical_and(occluded, rand<=ratio)).float()

    norm_w = (w/(torch.mean(w)+ epsilon)).reshape((w.size(0),1,w.size(1),w.size(2),w.size(3) )).repeat(1,12,1,1,1)


    # clip to prevent NaN's and Inf's
    #y_pred = torch.clip(y_pred, epsilon, 1 - epsilon)


    log_y_pred = torch.nn.LogSoftmax(dim=1)(y_pred)
    y_true = torch.movedim(torch.nn.functional.one_hot(y_true.type(torch.LongTensor), num_classes=12),4,1).to(weights.device)


    # calc
    #print(y_true.device)
    #print(log_y_pred.device)
    #print(norm_w.device)
    loss = -torch.mean(y_true * log_y_pred * norm_w)
    return loss


import argparse
from utils import path_config

# default settings
GPU = 0


# Dataloader settings
BATCH_SIZE = 4
VAL_BATCH_MULT = 2
WORKERS = 2
DATASET = "NYU"
PREPROC_PATH = ""
VOL_PATH = ""

# Model settings
WEIGHTS = "none"
BATCH_NORM = True
INSTANCE_NORM = False
INPUT_TYPE = "depth+rgb+normals"
FINE_TUNE = False
ORACLE=False


def parse_arguments():
    global GPU, \
           BATCH_SIZE, WORKERS, DATASET, PREPROC_PATH, VOL_PATH,\
           WEIGHTS, BATCH_NORM, INSTANCE_NORM, INPUT_TYPE, FINE_TUNE, ORACLE

    print("\nMMNet Evaluation Script\n")

    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", help="Target dataset", type=str, choices=['SUNCG', 'NYU', 'NYUCAD'])
    parser.add_argument("weights", help="Pretraind weights. ", type=str)
    parser.add_argument("--workers", help="Concurrent threads. Default " + str(WORKERS),
                        type=int, default=WORKERS, required=False)
    parser.add_argument("--gpu", help="GPU device. Default " + str(GPU),
                        type=int, default=GPU, required=False)
    parser.add_argument("--input_type",  help="Network input type. Default " + INPUT_TYPE,
                        type=str, default=INPUT_TYPE, required=False,
                        choices=['depth+rgb+normals', 'depth+rgb', 'depth']
                        )
    parser.add_argument("--bn",  help="Apply batch normalization?. Default yes",
                        type=str, default="yes", required=False,
                        choices=['yes', 'Yes', 'y', 'Y', 'no', 'No', 'n', 'N']
                        )
    parser.add_argument("--fine_tune",  help="Is it a fine tune from a suncg model?. Default no",
                        type=str, default="no", required=False,
                        choices=['yes', 'Yes', 'y', 'Y', 'no', 'No', 'n', 'N']
                        )
    parser.add_argument("--oracle",  help="Is it an oracle test?. Default no",
                        type=str, default="no", required=False,
                        choices=['yes', 'Yes', 'y', 'Y', 'no', 'No', 'n', 'N']
                        )

    args = parser.parse_args()

    DATASET = args.dataset
    WORKERS = args.workers
    GPU = args.gpu
    WEIGHTS = args.weights
    INPUT_TYPE = args.input_type
    BATCH_NORM = args.bn in ['yes', 'Yes', 'y', 'Y']
    FINE_TUNE = args.fine_tune in ['yes', 'Yes', 'y', 'Y']
    ORACLE = args.oracle in ['yes', 'Yes', 'y', 'Y']

    path_dict = path_config.read_config()
    '''
    if DATASET == "NYU":
        # VOL_PATH = path_dict["NYU_VOL_PATH"]
        if not FINE_TUNE:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["NYU_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYU_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["NYU_RGB_PRIOR_FINE_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYU_RGB_NORMALS_FINE_PRIOR_PREPROC"]

    elif DATASET == "NYUCAD":
        # VOL_PATH = path_dict["NYUCAD_VOL_PATH"]
        if not FINE_TUNE:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["NYUCAD_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYUCAD_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["NYUCAD_RGB_PRIOR_FINE_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYUCAD_RGB_NORMALS_FINE_PRIOR_PREPROC"]

    elif DATASET == "SUNCG":
        # VOL_PATH = path_dict["SUNCG_VOL_PATH"]
        if not FINE_TUNE:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["SUNCG_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["SUNCG_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            print("Fine-tuning from SUNCG is only possible with NYU or NYUCAD Datasets")
            exit(-1)
    else:
        print("Dataset", DATASET, "not supported yet!")
        exit(-1)
    '''
    if DATASET == "NYU":
        #VOL_PATH = path_dict["NYU_VOL_PATH"]
        if not FINE_TUNE:
            if INPUT_TYPE == "depth+rgb":
                PREPROC_PATH = path_dict["NYU_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYU_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            if INPUT_TYPE == "depth+rgb":
                PREPROC_PATH = path_dict["NYU_RGB_PRIOR_FINE_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYU_RGB_NORMALS_FINE_PRIOR_PREPROC"]

    elif DATASET == "NYUCAD":
        #VOL_PATH = path_dict["NYUCAD_VOL_PATH"]
        if not FINE_TUNE:
            if INPUT_TYPE == "depth+rgb":
                PREPROC_PATH = path_dict["NYUCAD_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYUCAD_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            if INPUT_TYPE == "depth+rgb":
                PREPROC_PATH = path_dict["NYUCAD_RGB_PRIOR_FINE_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYUCAD_RGB_NORMALS_FINE_PRIOR_PREPROC"]


    elif DATASET == "SUNCG":
        #VOL_PATH = path_dict["SUNCG_VOL_PATH"]
        if not FINE_TUNE:
            if INPUT_TYPE == "depth+rgb":
                PREPROC_PATH = path_dict["SUNCG_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["SUNCG_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            print("Fine-tuning from SUNCG is only possible with NYU or NYUCAD Datasets")
            exit(-1)
    else:
        print("Dataset", DATASET, "not supported yet!")
        exit(-1)


def augment(v1, i):
    import torch

    o1 = v1.clone()

    if i & 0b001:
        o1 = torch.transpose(o1, dim0=2, dim1=4).clone()
    if i & 0b010:
        o1 = torch.flip(o1, [2]).clone()

    if i & 0b100:
        o1 = torch.flip(o1, [4]).clone()

    return o1

def des_augment(v1, i):
    import torch

    o1 = v1.clone()

    if i & 0b100:
        o1 = torch.flip(o1, [4]).clone()

    if i & 0b010:
        o1 = torch.flip(o1, [2]).clone()

    if i & 0b001:
        o1 = torch.transpose(o1, dim0=2, dim1=4).clone()

    return o1


def eval():
    from tqdm import tqdm
    import os

    from utils.data import SSCMultimodalDataset, sample2dev
    from utils.data import get_file_prefixes_from_path
    from torch.utils.data import DataLoader
    import torch
    from utils.cuda import get_device
    import numpy as np
    from utils.metrics import MIoU, CompletionIoU
    from models.spawn import get_mmnet
    import h5py
    import glob

    nyu_classes = ["ceil", "floor", "wall", "window", "chair", "bed", "sofa", "table", "tvs", "furniture", "objects",
                   "empty"]

    print("Selected device:", "cuda:" + str(GPU))

    dev = get_device("cuda:" + str(GPU))
    torch.cuda.empty_cache()

    valid_prefixes = get_file_prefixes_from_path(os.path.join(PREPROC_PATH, "valid"), criteria="*.npz")

    valid_ds = SSCMultimodalDataset(valid_prefixes)

    dataloader = DataLoader(valid_ds, batch_size=BATCH_SIZE*VAL_BATCH_MULT, shuffle=False, num_workers=2)

    vox_shape = (240, 144, 240)
    vox_shape_down = (60, 36, 60)
    batch_shape = (1, 60, 36, 60)
    batch_shape_ch = (1, 60, 36, 60, 12)

    miou = MIoU(num_classes=12, ignore_class=0)

    ciou = CompletionIoU()

    model = get_mmnet(input_type=INPUT_TYPE, batch_norm=BATCH_NORM, inst_norm=False)

    print("loading", WEIGHTS)
    model.load_state_dict(torch.load(os.path.join("weights", WEIGHTS)))

    model.to(dev)
    model.eval()

    #torch.cuda.empty_cache()

    for phase in ['valid']:

        with torch.set_grad_enabled(phase == 'train'):

            with tqdm(total=len(dataloader), desc="") as pbar:
                for sample, prefix in zip(dataloader, valid_prefixes):
                    sample = sample2dev(sample, dev)

                    vox_tsdf = sample['vox_tsdf']
                    gt = sample['gt']
                    if not ORACLE:
                        vox_prior = sample['vox_prior']
                    else:
                        _, surf = torch.max(sample['vox_prior'], dim=1)
                        no_surf_idx = surf == 0
                        oracle_gt = gt.clone()
                        oracle_gt[no_surf_idx] = 0

                        # print("vp", sample['vox_prior'].shape)
                        # print("gt", gt.shape)
                        # print("oh_gt", torch.nn.functional.one_hot(gt.to(torch.int64), num_classes=12).shape)
                        vox_prior = torch.nn.functional.one_hot(oracle_gt.to(torch.int64), num_classes=12).to(
                            torch.float).transpose(3, 4).transpose(2, 3).transpose(1, 2)
                        # print("nvp",vox_prior.shape)
                    vox_weights = sample['vox_weights']

                    basename = os.path.basename(prefix)

                    # sscnet partial evaluation
                    #if DATASET != "SUNCG":
                    #    matfile = os.path.join(VOL_PATH, basename + '_vol_d4.mat')
                    #else:

                    #    search_criteria =  os.path.join(VOL_PATH, '*' + basename[:46] + '*_vol_d4.mat')
                    #    print(search_criteria)

                    #    result = glob.glob(search_criteria)
                    #    if len(result) == 1:
                    #        matfile = result[0]
                    #    else:
                    #        print("Unexpected list of matching mat files:")
                    #        print(result)
                    #        exit(-1)
                    #f = h5py.File(matfile, 'r')
                    #vol = np.array(f['flipVol_ds'])
                    #vol = torch.tensor((vol)).to(dev)
                    #vol = vol.view(list(vox_weights.size()))

                    #ciou_weights = vox_weights.clone()
                    #ciou_weights[(vol<0)&(vol>=-1)] = 0

                    #vox_weights[vol < -1] = 0  # Outside room as defined by SSCNET Song et. al. 2017
                    #vox_weights[vol == 1] = 0  # Outside room as defined by SSCNET Song et. al. 2017

                    pred = torch.zeros_like(vox_prior)

                    for i in range(8):

                        vox_tsdf_aug, vox_prior_aug  = augment(vox_tsdf, i), augment(vox_prior, i)

                        pred += des_augment(model(vox_tsdf_aug, vox_prior_aug),i)

                    miou.update(pred, gt, vox_weights)
                    #miou.update(pred, gt, vol=vol)

                    #pred2 = pred.clone()
                    #pred2[:,:,1:] = pred[:,:,:-1]
                    #pred2[:,:,-1] = 2


                    #ciou.update(pred, gt, vol=vol)
                    ciou.update(pred, gt, vox_weights)#, vol=vol)
                    #ciou.update(pred, gt, vol=vol)

                    pbar.set_description('{} miou:{:5.1f}'.format(phase, miou.compute() * 100))
                    pbar.update()

        comp_iou, precision, recall = ciou.compute()

        print("prec rec. IoU  MIou")
        print("{:4.1f} {:4.1f} {:4.1f} {:4.1f}".format(100 * precision, 100 * recall, 100 * comp_iou,
                                                       miou.compute() * 100))

        per_class_iou = miou.per_class_iou()
        for i in range(len(per_class_iou)):
            text = '{:12.12}: {:5.1f}'.format(nyu_classes[i], 100 * per_class_iou[i])
            print(text, end="        ")
            if i % 4 == 3:
                print()

        print("\nLatex Line:")
        print("{:4.1f} & {:4.1f} & {:4.1f} &".format(100 * precision, 100 * recall, 100 * comp_iou), end=" ")
        for i in range(len(per_class_iou)):
            text = '{:4.1f} &'.format(100 * per_class_iou[i])
            print(text, end=" ")

        print("{:4.1f} \\\\".format(miou.compute() * 100))


# Main Function
def main():
    parse_arguments()
    eval()


if __name__ == '__main__':
  main()


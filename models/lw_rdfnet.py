import torch.nn as nn
import torch.nn.functional as F
import torchvision.models as models
from collections import OrderedDict

def batchnorm(in_planes):
    # "batch norm 2d"
    return nn.BatchNorm2d(in_planes, affine=True, eps=1e-5, momentum=0.1)


def conv3x3(in_planes, out_planes, stride=1, bias=False):
    # "3x3 convolution with padding"
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=bias)


def conv1x1(in_planes, out_planes, stride=1, bias=False):
    # "1x1 convolution"
    return nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride,
                     padding=0, bias=bias)


def convbnrelu(in_planes, out_planes, kernel_size, stride=1, groups=1, act=True):
    # "conv-batchnorm-relu"
    if act:
        return nn.Sequential(
            nn.Conv2d(in_planes, out_planes, kernel_size, stride=stride, padding=int(kernel_size / 2.), groups=groups,
                      bias=False),
            batchnorm(out_planes),
            nn.ReLU6(inplace=True))
    else:
        return nn.Sequential(
            nn.Conv2d(in_planes, out_planes, kernel_size, stride=stride, padding=int(kernel_size / 2.), groups=groups,
                      bias=False),
            batchnorm(out_planes))


class CRPBlock(nn.Module):

    def __init__(self, in_planes, out_planes, n_stages):
        super(CRPBlock, self).__init__()
        for i in range(n_stages):
            setattr(self, '{}_{}'.format(i + 1, 'outvar_dimred'),
                    conv3x3(in_planes if (i == 0) else out_planes,
                            out_planes, stride=1,
                            bias=False))
        self.stride = 1
        self.n_stages = n_stages
        self.maxpool = nn.MaxPool2d(kernel_size=5, stride=1, padding=2)

    def forward(self, x):
        # ?x = F.relu(x)?
        top = x
        for i in range(self.n_stages):
            top = self.maxpool(top)
            top = getattr(self, '{}_{}'.format(i + 1, 'outvar_dimred'))(top)
            x = top + x
        return x


stages_suffixes = {0: '_conv',
                   1: '_conv_relu_varout_dimred'}


class RCUBlock(nn.Module):

    def __init__(self, in_planes, out_planes, n_blocks, n_stages):
        super(RCUBlock, self).__init__()
        for i in range(n_blocks):
            for j in range(n_stages):
                setattr(self, '{}{}'.format(i + 1, stages_suffixes[j]),
                        conv3x3(in_planes if (i == 0) and (j == 0) else out_planes,
                                out_planes, stride=1,
                                bias=(j == 0)))
        self.stride = 1
        self.n_blocks = n_blocks
        self.n_stages = n_stages

    def forward(self, x):
        for i in range(self.n_blocks):
            residual = x
            for j in range(self.n_stages):
                x = F.relu(x)
                x = getattr(self, '{}{}'.format(i + 1, stages_suffixes[j]))(x)
            x += residual
        return x

class BiModalMMFBlock(nn.Module):

    def __init__(self, in_planes, out_planes):
        super(BiModalMMFBlock, self).__init__()
        self.mode1_branch = nn.Sequential(OrderedDict([
                                              ('b1_dim_red', conv3x3(in_planes, out_planes, stride=1, bias=True)),
                                              ('b1_rcu', RCUBlock(out_planes, out_planes, 1, 2)),
                                              ('b1_conv', conv3x3(out_planes, out_planes, stride=1, bias=True))
                                            ]))
        self.mode2_branch = nn.Sequential(OrderedDict([
                                              ('b2_dim_red', conv3x3(in_planes, out_planes, stride=1, bias=True)),
                                              ('b2_rcu', RCUBlock(out_planes, out_planes, 1, 2)),
                                              ('b2_conv', conv3x3(out_planes, out_planes, stride=1, bias=True))
                                            ]))

    def forward(self, x1, x2):
        m1 = self.mode1_branch(x1)
        m2 = self.mode2_branch(x2)
        out = F.relu(m1+m2)
        return out


class TriModalMMFBlock(nn.Module):

    def __init__(self, in_planes, out_planes):
        super(TriModalMMFBlock, self).__init__()
        self.mode1_branch = nn.Sequential(OrderedDict([
                                              ('b1_dim_red', conv3x3(in_planes, out_planes, stride=1, bias=True)),
                                              ('b1_rcu', RCUBlock(out_planes, out_planes, 1, 2)),
                                              ('b1_conv', conv3x3(out_planes, out_planes, stride=1, bias=True))
                                            ]))
        self.mode2_branch = nn.Sequential(OrderedDict([
                                              ('b2_dim_red', conv3x3(in_planes, out_planes, stride=1, bias=True)),
                                              ('b2_rcu', RCUBlock(out_planes, out_planes, 1, 2)),
                                              ('b2_conv', conv3x3(out_planes, out_planes, stride=1, bias=True))
                                            ]))
        self.mode3_branch = nn.Sequential(OrderedDict([
                                              ('b3_dim_red', conv3x3(in_planes, out_planes, stride=1, bias=True)),
                                              ('b3_rcu', RCUBlock(out_planes, out_planes, 1, 2)),
                                              ('b3_conv', conv3x3(out_planes, out_planes, stride=1, bias=True))
                                            ]))

    def forward(self, x1, x2, x3):
        m1 = self.mode1_branch(x1)
        m2 = self.mode2_branch(x2)
        m3 = self.mode3_branch(x3)
        out = F.relu(m1+m2+m3)
        return out


class BiModalRDFNetLW(nn.Module):

    def __init__(self, num_classes=11):
        super(BiModalRDFNetLW, self).__init__()

        # get a pretrained encoder
        self.encoder1 = models.resnet101(pretrained=True)
        del self.encoder1.fc
        del self.encoder1.avgpool

        self.encoder2 = models.resnet101(pretrained=True)
        del self.encoder2.fc
        del self.encoder2.avgpool

        self.do = nn.Dropout(p=0.5)
        self.relu = nn.ReLU(inplace=True)

        self.mmf4 = BiModalMMFBlock(2048, 512)
        self.mflow_conv_g1_pool = self._make_crp(512, 512, 4)
        self.mflow_conv_g1_b3_joint_varout_dimred = conv3x3(512, 256, bias=False)

        self.mmf3 = BiModalMMFBlock(1024, 256)
        self.adapt_stage2_b2_joint_varout_dimred = conv3x3(256, 256, bias=False)
        self.mflow_conv_g2_pool = self._make_crp(256, 256, 4)
        self.mflow_conv_g2_b3_joint_varout_dimred = conv3x3(256, 256, bias=False)

        self.mmf2 = BiModalMMFBlock(512, 256)
        self.adapt_stage3_b2_joint_varout_dimred = conv3x3(256, 256, bias=False)
        self.mflow_conv_g3_pool = self._make_crp(256, 256, 4)
        self.mflow_conv_g3_b3_joint_varout_dimred = conv3x3(256, 256, bias=False)

        self.mmf1 = BiModalMMFBlock(256, 256)
        self.adapt_stage4_b2_joint_varout_dimred = conv3x3(256, 256, bias=False)
        self.mflow_conv_g4_pool = self._make_crp(256, 256, 4)

        self.clf_conv = nn.Conv2d(256, num_classes, kernel_size=3, stride=1,
                                  padding=1, bias=True)

    def _make_crp(self, in_planes, out_planes, stages):
        layers = [CRPBlock(in_planes, out_planes, stages)]
        return nn.Sequential(*layers)

    def _make_rcu(self, in_planes, out_planes, blocks, stages):
        layers = [RCUBlock(in_planes, out_planes, blocks, stages)]
        return nn.Sequential(*layers)

    def forward(self, x1, x2):
        # get outputs from encoder1
        x_1 = self.encoder1.conv1(x1)
        x_1 = self.encoder1.bn1(x_1)
        x_1 = self.encoder1.relu(x_1)
        x_1 = self.encoder1.maxpool(x_1)
        l1_1 = self.encoder1.layer1(x_1)
        l2_1 = self.encoder1.layer2(l1_1)
        l3_1 = self.encoder1.layer3(l2_1)
        l4_1 = self.encoder1.layer4(l3_1)

        # get outputs from encoder2
        x_2 = self.encoder2.conv1(x2)
        x_2 = self.encoder2.bn1(x_2)
        x_2 = self.encoder2.relu(x_2)
        x_2 = self.encoder2.maxpool(x_2)
        l1_2 = self.encoder2.layer1(x_2)
        l2_2 = self.encoder2.layer2(l1_2)
        l3_2 = self.encoder2.layer3(l2_2)
        l4_2 = self.encoder2.layer4(l3_2)

        # apply dropout
        l4_1 = self.do(l4_1)
        l3_1 = self.do(l3_1)
        l4_2 = self.do(l4_2)
        l3_2 = self.do(l3_2)

        # MMF and refineNet blocks
        x4 = self.mmf4(l4_1, l4_2)
        x4 = self.relu(x4)
        x4 = self.mflow_conv_g1_pool(x4)
        x4 = self.mflow_conv_g1_b3_joint_varout_dimred(x4)
        x4 = nn.Upsample(size=l3_1.size()[2:], mode='bilinear', align_corners=True)(x4)

        x3 = self.mmf3(l3_1, l3_2)
        x3 = self.adapt_stage2_b2_joint_varout_dimred(x3)
        x3 = x3 + x4
        x3 = F.relu(x3)
        x3 = self.mflow_conv_g2_pool(x3)
        x3 = self.mflow_conv_g2_b3_joint_varout_dimred(x3)
        x3 = nn.Upsample(size=l2_1.size()[2:], mode='bilinear', align_corners=True)(x3)

        x2 = self.mmf2(l2_1, l2_2)
        x2 = self.adapt_stage3_b2_joint_varout_dimred(x2)
        x2 = x2 + x3
        x2 = F.relu(x2)
        x2 = self.mflow_conv_g3_pool(x2)
        x2 = self.mflow_conv_g3_b3_joint_varout_dimred(x2)
        x2 = nn.Upsample(size=l1_1.size()[2:], mode='bilinear', align_corners=True)(x2)

        x1 = self.mmf1(l1_1, l1_2)
        x1 = self.adapt_stage4_b2_joint_varout_dimred(x1)
        x1 = x1 + x2
        x1 = F.relu(x1)
        x1 = self.mflow_conv_g4_pool(x1)

        out = self.clf_conv(x1)

        return out

class TriModalRDFNetLW(nn.Module):

    def __init__(self, num_classes=11):
        super(TriModalRDFNetLW, self).__init__()

        # get a pretrained encoder
        self.encoder1 = models.resnet101(pretrained=True)
        del self.encoder1.fc
        del self.encoder1.avgpool

        self.encoder2 = models.resnet101(pretrained=True)
        del self.encoder2.fc
        del self.encoder2.avgpool

        self.encoder3 = models.resnet101(pretrained=True)
        del self.encoder3.fc
        del self.encoder3.avgpool

        self.do = nn.Dropout(p=0.5)
        self.relu = nn.ReLU(inplace=True)

        self.mmf4 = TriModalMMFBlock(2048, 512)
        self.mflow_conv_g1_pool = self._make_crp(512, 512, 4)
        self.mflow_conv_g1_b3_joint_varout_dimred = conv3x3(512, 256, bias=False)

        self.mmf3 = TriModalMMFBlock(1024, 256)
        self.adapt_stage2_b2_joint_varout_dimred = conv3x3(256, 256, bias=False)
        self.mflow_conv_g2_pool = self._make_crp(256, 256, 4)
        self.mflow_conv_g2_b3_joint_varout_dimred = conv3x3(256, 256, bias=False)

        self.mmf2 = TriModalMMFBlock(512, 256)
        self.adapt_stage3_b2_joint_varout_dimred = conv3x3(256, 256, bias=False)
        self.mflow_conv_g3_pool = self._make_crp(256, 256, 4)
        self.mflow_conv_g3_b3_joint_varout_dimred = conv3x3(256, 256, bias=False)

        self.mmf1 = TriModalMMFBlock(256, 256)
        self.adapt_stage4_b2_joint_varout_dimred = conv3x3(256, 256, bias=False)
        self.mflow_conv_g4_pool = self._make_crp(256, 256, 4)

        self.clf_conv = nn.Conv2d(256, num_classes, kernel_size=3, stride=1,
                                  padding=1, bias=True)

    def _make_crp(self, in_planes, out_planes, stages):
        layers = [CRPBlock(in_planes, out_planes, stages)]
        return nn.Sequential(*layers)

    def _make_rcu(self, in_planes, out_planes, blocks, stages):
        layers = [RCUBlock(in_planes, out_planes, blocks, stages)]
        return nn.Sequential(*layers)

    def forward(self, x1, x2, x3):
        # get outputs from encoder1
        x_1 = self.encoder1.conv1(x1)
        x_1 = self.encoder1.bn1(x_1)
        x_1 = self.encoder1.relu(x_1)
        x_1 = self.encoder1.maxpool(x_1)
        l1_1 = self.encoder1.layer1(x_1)
        l2_1 = self.encoder1.layer2(l1_1)
        l3_1 = self.encoder1.layer3(l2_1)
        l4_1 = self.encoder1.layer4(l3_1)

        # get outputs from encoder2
        x_2 = self.encoder2.conv1(x2)
        x_2 = self.encoder2.bn1(x_2)
        x_2 = self.encoder2.relu(x_2)
        x_2 = self.encoder2.maxpool(x_2)
        l1_2 = self.encoder2.layer1(x_2)
        l2_2 = self.encoder2.layer2(l1_2)
        l3_2 = self.encoder2.layer3(l2_2)
        l4_2 = self.encoder2.layer4(l3_2)

        # get outputs from encoder2
        x_3 = self.encoder2.conv1(x3)
        x_3 = self.encoder2.bn1(x_3)
        x_3 = self.encoder2.relu(x_3)
        x_3 = self.encoder2.maxpool(x_3)
        l1_3 = self.encoder2.layer1(x_3)
        l2_3 = self.encoder2.layer2(l1_3)
        l3_3 = self.encoder2.layer3(l2_3)
        l4_3 = self.encoder2.layer4(l3_3)

        # apply dropout
        l4_1 = self.do(l4_1)
        l3_1 = self.do(l3_1)
        l4_2 = self.do(l4_2)
        l3_2 = self.do(l3_2)
        l4_3 = self.do(l4_3)
        l3_3 = self.do(l3_3)

        # MMF and refineNet blocks
        x4 = self.mmf4(l4_1, l4_2, l4_3)
        x4 = self.relu(x4)
        x4 = self.mflow_conv_g1_pool(x4)
        x4 = self.mflow_conv_g1_b3_joint_varout_dimred(x4)
        x4 = nn.Upsample(size=l3_1.size()[2:], mode='bilinear', align_corners=True)(x4)

        x3 = self.mmf3(l3_1, l3_2, l3_3)
        x3 = self.adapt_stage2_b2_joint_varout_dimred(x3)
        x3 = x3 + x4
        x3 = F.relu(x3)
        x3 = self.mflow_conv_g2_pool(x3)
        x3 = self.mflow_conv_g2_b3_joint_varout_dimred(x3)
        x3 = nn.Upsample(size=l2_1.size()[2:], mode='bilinear', align_corners=True)(x3)

        x2 = self.mmf2(l2_1, l2_2, l2_3)
        x2 = self.adapt_stage3_b2_joint_varout_dimred(x2)
        x2 = x2 + x3
        x2 = F.relu(x2)
        x2 = self.mflow_conv_g3_pool(x2)
        x2 = self.mflow_conv_g3_b3_joint_varout_dimred(x2)
        x2 = nn.Upsample(size=l1_1.size()[2:], mode='bilinear', align_corners=True)(x2)

        x1 = self.mmf1(l1_1, l1_2, l1_3)
        x1 = self.adapt_stage4_b2_joint_varout_dimred(x1)
        x1 = x1 + x2
        x1 = F.relu(x1)
        x1 = self.mflow_conv_g4_pool(x1)

        out = self.clf_conv(x1)

        return out



def get_bimodal_RDFNet(num_classes=11):

    model = BiModalRDFNetLW(num_classes)

    decoder_parameters = []
    encoder_parameters = []
    for name, param in model.named_parameters():
        if param.requires_grad:
            if name[:7] == "encoder":
                encoder_parameters.append(param)
            else:
                decoder_parameters.append(param)
    return model, encoder_parameters, decoder_parameters

def get_trimodal_RDFNet(num_classes=11):

    model = TriModalRDFNetLW(num_classes)

    decoder_parameters = []
    encoder_parameters = []
    for name, param in model.named_parameters():
        if param.requires_grad:
            if name[:7] == "encoder":
                encoder_parameters.append(param)
            else:
                decoder_parameters.append(param)
    return model, encoder_parameters, decoder_parameters

from keras.models import Model
from keras.layers import Input, concatenate, Conv3D, Add, MaxPooling3D, Activation, BatchNormalization, UpSampling3D
from keras.layers import Conv3DTranspose
from keras.initializers import RandomNormal

def get_sscnet_trunk(x):
    # Conv1

    conv1 = Conv3D(16, 7, strides=2, dilation_rate=1, padding='same', name='conv_1_1', activation='relu')(x)
    conv1 = Conv3D(32, 3, strides=1, dilation_rate=1, padding='same', name='conv_1_2', activation='relu')(conv1)
    conv1 = Conv3D(32, 3, strides=1, dilation_rate=1, padding='same', name='conv_1_3')(conv1)


    add1 = Conv3D(32, 1, strides=1, dilation_rate=1, padding='same', name='red_1')(conv1) #reduction
    add1 = Add()([conv1, add1])
    add1 = Activation('relu')(add1)

    pool1 = MaxPooling3D(2, strides=2)(add1)

    # Conv2

    conv2 = Conv3D(64, 3, strides=1, dilation_rate=1, padding='same', name='conv_2_1', activation='relu')(pool1)
    conv2 = Conv3D(64, 3, strides=1, dilation_rate=1, padding='same', name='conv_2_2', activation='relu')(conv2)

    add2 = Conv3D(64, 1, strides=1, dilation_rate=1, padding='same', name='red_2')(pool1) #reduction
    add2 = Add()([conv2, add2])
    add2 = Activation('relu')(add2)
    add2 = Activation('relu')(add2) # 2 ativações?

    # Conv3

    conv3 = Conv3D(64, 3, strides=1, dilation_rate=1, padding='same', name='conv_3_1', activation='relu')(add2)
    conv3 = Conv3D(64, 3, strides=1, dilation_rate=1, padding='same', name='conv_3_2', activation='relu')(conv3)

    add3 = Add()([conv3, add2])
    add3 = Activation('relu')(add3)
    add3 = Activation('relu')(add3) # 2 ativações?

    # Dilated1

    dil1 = Conv3D(64, 3, strides=1, dilation_rate=2, padding='same', name='dil_1_1', activation='relu')(add3)
    dil1 = Conv3D(64, 3, strides=1, dilation_rate=2, padding='same', name='dil_1_2', activation='relu')(dil1)

    add4 = Add()([dil1, add3])
    add4 = Activation('relu')(add4)
    add4 = Activation('relu')(add4)

    # Dilated2

    dil2 = Conv3D(64, 3, strides=1, dilation_rate=2, padding='same', name='dil_2_1', activation='relu')(add4)
    dil2 = Conv3D(64, 3, strides=1, dilation_rate=2, padding='same', name='dil_2_2', activation='relu')(dil2)

    add5 = Add()([dil2, add4])
    add5 = Activation('relu')(add5)

    # Concat

    conc = concatenate([add2, add3, add4, add5], axis=4)

    # Final Convolutions

    init1 = RandomNormal(mean=0.0, stddev=0.01, seed=None)
    fin = Conv3D(128, 1, padding='same', name='fin_1', activation='relu', kernel_initializer=init1 )(conc)

    init2 = RandomNormal(mean=0.0, stddev=0.01, seed=None)
    fin = Conv3D(128, 1, padding='same', name='fin_2', activation='relu', kernel_initializer=init2 )(fin)

    init3 = RandomNormal(mean=0.0, stddev=0.01, seed=None)
    fin = Conv3D(12, 1, padding='same', name='fin_3', activation='softmax', kernel_initializer=init3 )(fin)

    return fin


def get_sscnet():
    input_tsdf = Input(shape=(240, 144, 240, 1)) #channels last

    fin = get_sscnet_trunk(input_tsdf)

    model = Model(inputs=input_tsdf, outputs=fin)

    return model

def get_csscnet():
    input_tsdf = Input(shape=(240, 144, 240, 1)) #channels last
    input_rgb = Input(shape=(240, 144, 240, 3)) #channels last

    x = concatenate([input_tsdf,input_rgb],axis=-1)

    fin = get_sscnet_trunk(x)

    model = Model(inputs=[input_tsdf,input_rgb], outputs=fin)

    return model

def get_sscnet_edges():
    input_tsdf = Input(shape=(240, 144, 240, 1))
    input_edges = Input(shape=(240, 144, 240, 1))

    x = concatenate([input_tsdf, input_edges], axis=-1)

    fin = get_sscnet_trunk(x)

    model = Model(inputs=[input_tsdf, input_edges], outputs=fin)

    return model


def get_res_unet_u(y):

    print("get res unet u")


    x = Conv3D(32, (3, 3, 3), padding='same')(y)

    down3 = BatchNormalization()(x)
    down3 = Activation('relu')(down3)
    down3 = Conv3D(32, (3, 3, 3), padding='same')(down3)
    down3 = BatchNormalization()(down3)
    down3 = Activation('relu')(down3)
    down3 = Conv3D(32, (3, 3, 3), padding='same')(down3)
    down3 = Add()([x,down3])
    down3_pool = MaxPooling3D((2, 2, 2), strides=(2, 2, 2))(down3)
    # 30 x 18 x 30

    x = Conv3D(64, (3, 3, 3), padding='same')(down3_pool)

    down4 = BatchNormalization()(x)
    down4 = Activation('relu')(down4)
    down4 = Conv3D(64, (3, 3, 3), padding='same', dilation_rate=2)(down4)
    down4 = BatchNormalization()(down4)
    down4 = Activation('relu')(down4)
    down4 = Conv3D(64, (3, 3, 3), padding='same', dilation_rate=2)(down4)
    res = Add()([x,down4])

    down4 = BatchNormalization()(res)
    down4 = Activation('relu')(down4)
    down4 = Conv3D(64, (3, 3, 3), padding='same', dilation_rate=2)(down4)
    down4 = BatchNormalization()(down4)
    down4 = Activation('relu')(down4)
    down4 = Conv3D(64, (3, 3, 3), padding='same', dilation_rate=2)(down4)
    down4 = Add()([res,down4])

    down4_pool = MaxPooling3D((2, 2, 2), strides=(2, 2, 2))(down4)
    # 15 x 9 x 15

    x = Conv3D(128, (3, 3, 3), padding='same')(down4_pool)

    center = BatchNormalization()(x)
    center = Activation('relu')(center)
    center = Conv3D(128, (3, 3, 3), padding='same', dilation_rate=2)(center)
    center = BatchNormalization()(center)
    center = Activation('relu')(center)
    center = Conv3D(128, (3, 3, 3), padding='same', dilation_rate=2)(center)
    res = Add()([x,center])

    center = Activation('relu')(res)
    center = Conv3D(128, (3, 3, 3), padding='same', dilation_rate=2)(center)
    center = BatchNormalization()(center)
    center = Activation('relu')(center)
    center = Conv3D(128, (3, 3, 3), padding='same', dilation_rate=2)(center)
    center = Add()([res,center])
    # center

    up4 = Conv3DTranspose(64, (2, 2, 2), strides=(2,2,2))(center)
    res = concatenate([down4, up4], axis=-1)

    res = Conv3D(64, (3, 3, 3), padding='same')(res)

    up4 = BatchNormalization()(res)
    up4 = Activation('relu')(up4)
    up4 = Conv3D(64, (3, 3, 3), padding='same')(up4)
    up4 = BatchNormalization()(up4)
    up4 = Activation('relu')(up4)
    up4 = Conv3D(64, (3, 3, 3), padding='same')(up4)
    res = Add() ([res,up4])

    up4 = BatchNormalization()(res)
    up4 = Activation('relu')(up4)
    up4 = Conv3D(64, (3, 3, 3), padding='same')(up4)
    up4 = BatchNormalization()(up4)
    up4 = Activation('relu')(up4)
    up4 = Conv3D(64, (3, 3, 3), padding='same')(up4)
    up4 = Add() ([res,up4])
    # 30 x 18 x 30

    up3 = Conv3DTranspose(32, (2, 2, 2), strides=(2,2,2))(up4)
    res = concatenate([down3, up3], axis=-1)

    res = Conv3D(32, (3, 3, 3), padding='same')(res)

    up3 = BatchNormalization()(res)
    up3 = Activation('relu')(up3)
    up3 = Conv3D(32, (3, 3, 3), padding='same')(up3)
    up3 = BatchNormalization()(up3)
    up3 = Activation('relu')(up3)
    up3 = Conv3D(32, (3, 3, 3), padding='same')(up3)
    up3 = Add() ([res,up3])

    # 60 x 36 60

    fin = concatenate([y, up3], axis=-1)
    fin = Conv3D(16, 1, padding='same', name='fin_1', activation='relu')(fin)
    fin = Conv3D(16, 1, padding='same', name='fin_2', activation='relu')(fin)
    fin = Conv3D(12, 1, padding='same', name='fin_3', activation='softmax' )(fin)

    return fin


def get_resnet_module(channels, x):
    down = BatchNormalization()(x)
    down = Activation('relu')(down)
    down = Conv3D(channels, (3, 3, 3), padding='same')(down)
    down = BatchNormalization()(down)
    down = Activation('relu')(down)
    down = Conv3D(channels, (3, 3, 3), padding='same')(down)
    down = Add()([x,down])
    return down

def get_dilated_resnet_module(channels, x):
    down = BatchNormalization()(x)
    down = Activation('relu')(down)
    down = Conv3D(channels, (3, 3, 3), padding='same', dilation_rate=2)(down)
    down = BatchNormalization()(down)
    down = Activation('relu')(down)
    down = Conv3D(channels, (3, 3, 3), padding='same', dilation_rate=2)(down)
    res = Add()([x,down])
    return res


def get_res_unet_u_mf(d, e, decode_branch):

    x_d = Conv3D(decode_branch, (3, 3, 3), padding='same')(d)
    x_e = Conv3D(decode_branch, (3, 3, 3), padding='same')(e)

    down3_d = get_resnet_module(decode_branch,x_d)
    down3_e = get_resnet_module(decode_branch,x_e)

    down3_pool_d = MaxPooling3D((2, 2, 2), strides=(2, 2, 2))(down3_d)
    down3_pool_e = MaxPooling3D((2, 2, 2), strides=(2, 2, 2))(down3_e)
    # 30 x 18 x 30


    x_d = Conv3D(decode_branch*2, (3, 3, 3), padding='same')(down3_pool_d)
    x_e = Conv3D(decode_branch*2, (3, 3, 3), padding='same')(down3_pool_e)

    res_d = get_dilated_resnet_module(decode_branch*2,x_d)
    res_e = get_dilated_resnet_module(decode_branch*2,x_e)

    down4_d = get_dilated_resnet_module(decode_branch*2,res_d)
    down4_e = get_dilated_resnet_module(decode_branch*2,res_e)

    down4_pool_d = MaxPooling3D((2, 2, 2), strides=(2, 2, 2))(down4_d)
    down4_pool_e = MaxPooling3D((2, 2, 2), strides=(2, 2, 2))(down4_e)
    # 15 x 9 x 15

    down4_pool = concatenate([down4_pool_d, down4_pool_e], axis=-1)


    x = Conv3D(128, (3, 3, 3), padding='same')(down4_pool)

    res = get_dilated_resnet_module(128,x)
    center = get_dilated_resnet_module(128,res)
    # center

    up4 = Conv3DTranspose(64, (2, 2, 2), strides=(2,2,2))(center)
    res = concatenate([down4_d, down4_e, up4], axis=-1)

    res = Conv3D(64, (3, 3, 3), padding='same')(res)

    res = get_resnet_module(64,res)
    up4 = get_resnet_module(64,res)
    # 30 x 18 x 30

    up3 = Conv3DTranspose(32, (2, 2, 2), strides=(2,2,2))(up4)
    res = concatenate([down3_d, down3_e, up3], axis=-1)

    res = Conv3D(32, (3, 3, 3), padding='same')(res)

    up3 = get_resnet_module(32,res)
    # 60 x 36 60

    fin = concatenate([d, e, up3], axis=-1)
    fin = Conv3D(16, 1, padding='same', name='fin_1', activation='relu')(fin)
    fin = Conv3D(16, 1, padding='same', name='fin_2', activation='relu')(fin)
    fin = Conv3D(12, 1, padding='same', name='fin_3', activation='softmax' )(fin)

    return fin


def get_res_unet_trunk(x):

    print("get resunet trunk")


    x = Conv3D(8, (3, 3, 3), padding='same', name="C0_tr_input")(x)

    down1 = BatchNormalization(name="BN11_tr_input")(x)
    down1 = Activation('relu',name="AC11_tr_input")(down1)
    down1 = Conv3D(8, (3, 3, 3), padding='same',name="C11_tr_input")(down1)
    down1 = BatchNormalization(name="BN12_tr_input")(down1)
    down1 = Activation('relu',name="AC12_tr_input")(down1)
    down1 = Conv3D(8, (3, 3, 3), padding='same',name="C12_tr_input")(down1)
    down1 = Add(name="ADD1_tr_input")([x,down1])
    down1_pool = MaxPooling3D((2, 2, 2), strides=(2, 2, 2),name="MP1_tr_input")(down1)
    # 120 x 72 x 120


    x = Conv3D(16, (3, 3, 3), padding='same')(down1_pool)

    down2 = BatchNormalization(name="BN21_tr_input")(x)
    down2 = Activation('relu',name="AC21_tr_input")(down2)
    down2 = Conv3D(16, (3, 3, 3), padding='same',name="C21_tr_input")(down2)
    down2 = BatchNormalization(name="BN22_tr_input")(down2)
    down2 = Activation('relu',name="AC22_tr_input")(down2)
    down2 = Conv3D(16, (3, 3, 3), padding='same',name="C22_tr_input")(down2)
    down2 = Add(name="ADD2_tr_input")([x,down2])
    down2_pool = MaxPooling3D((2, 2, 2), strides=(2, 2, 2),name="MP2_tr_input")(down2)
    # 60 x 36 x 60

    return get_res_unet_u(down2_pool)


def get_full_input_branch(inp,half, initial):
    x = Conv3D(initial, (3, 3, 3), padding='same', name="C0_tr_input"+"_half"+str(half))(inp)

    down1 = BatchNormalization(name="BN11_tr_input"+"_half"+str(half))(x)
    down1 = Activation('relu',name="AC11_tr_input"+"_half"+str(half))(down1)
    down1 = Conv3D(initial, (3, 3, 3), padding='same',name="C11_tr_input"+"_half"+str(half))(down1)
    down1 = BatchNormalization(name="BN12_tr_input"+"_half"+str(half))(down1)
    down1 = Activation('relu',name="AC12_tr_input"+"_half"+str(half))(down1)
    down1 = Conv3D(initial, (3, 3, 3), padding='same',name="C12_tr_input"+"_half"+str(half))(down1)
    down1 = Add(name="ADD1_tr_input"+"_half"+str(half))([x,down1])
    down1_pool = MaxPooling3D((2, 2, 2), strides=(2, 2, 2),name="MP1_tr_input"+"_half"+str(half))(down1)
    # 120 x 72 x 120


    x = Conv3D(initial*2, (3, 3, 3), padding='same')(down1_pool)

    down2 = BatchNormalization(name="BN21_tr_input"+"_half"+str(half))(x)
    down2 = Activation('relu',name="AC21_tr_input"+"_half"+str(half))(down2)
    down2 = Conv3D(initial*2, (3, 3, 3), padding='same',name="C21_tr_input"+"_half"+str(half))(down2)
    down2 = BatchNormalization(name="BN22_tr_input"+"_half"+str(half))(down2)
    down2 = Activation('relu',name="AC22_tr_input"+"_half"+str(half))(down2)
    down2 = Conv3D(initial*2, (3, 3, 3), padding='same',name="C22_tr_input"+"_half"+str(half))(down2)
    down2 = Add(name="ADD2_tr_input"+"_half"+str(half))([x,down2])
    down2_pool = MaxPooling3D((2, 2, 2), strides=(2, 2, 2),name="MP2_tr_input"+"_half"+str(half))(down2)
    # 60 x 36 x 60
    return down2_pool


def get_res_unet_mf_trunk(d, e, input_branch, decode_branch):

    d = get_full_input_branch(e,half=1, initial=input_branch)
    e = get_full_input_branch(e,half=2, initial=input_branch)

    return get_res_unet_u_mf(d, e, decode_branch)


def get_res_unet_mf2_trunk(d, e, input_branch):

    d = get_full_input_branch(d,half=1, initial=input_branch)
    e = get_full_input_branch(e,half=2, initial=input_branch)

    x = concatenate([d, e], axis=-1)

    return get_res_unet_u(x)


def get_res_unet_edges():
    input_tsdf = Input(shape=(240, 144, 240, 1))
    input_edges = Input(shape=(240, 144, 240, 1))

    x = concatenate([input_tsdf, input_edges], axis=-1)

    fin = get_res_unet_trunk(x)

    model = Model(inputs=[input_tsdf, input_edges], outputs=fin)

    return model


def get_res_unet_edges_mf(input_branch, decode_branch):
    input_tsdf = Input(shape=(240, 144, 240, 1))
    input_edges = Input(shape=(240, 144, 240, 1))

    fin = get_res_unet_mf_trunk(input_tsdf,input_edges, input_branch, decode_branch)

    model = Model(inputs=[input_tsdf, input_edges], outputs=fin)

    return model


def get_res_unet_edges_mf2(input_branch):
    input_tsdf = Input(shape=(240, 144, 240, 1))
    input_edges = Input(shape=(240, 144, 240, 1))

    fin = get_res_unet_mf2_trunk(input_tsdf,input_edges, input_branch)

    model = Model(inputs=[input_tsdf, input_edges], outputs=fin)

    return model


def get_res_unet():
    input_tsdf = Input(shape=(240, 144, 240, 1))

    fin = get_res_unet_trunk(input_tsdf)

    model = Model(inputs=input_tsdf, outputs=fin)

    return model

def get_network_by_name(name):

    print("get network by name", name)

    if name == 'SSCNET':
        return get_sscnet(), 'depth'
    elif name == 'SSCNET_E':
        return get_sscnet_edges(), 'depth+edges'
    elif name == 'EDGENET_EF':
        return get_res_unet_edges(), 'depth+edges'
    elif name == 'EDGENET_MF':
        return get_res_unet_edges_mf(input_branch=4, decode_branch=16), 'depth+edges'
    elif name == 'EDGENET_MF_D':
        return get_res_unet_edges_mf(input_branch=8, decode_branch=32), 'depth+edges'
    elif name == 'EDGENET_MF2':
        return get_res_unet_edges_mf2(input_branch=4), 'depth+edges'
    elif name == 'EDGENET_MF2_D':
        return get_res_unet_edges_mf2(input_branch=8), 'depth+edges'
    elif name == 'EDGENET_D':
        return get_res_unet(), 'depth'
    elif name == 'EDGENET_E':
        return get_res_unet(), 'edges'


def get_net_name_from_w(weights):
    networks = ['SSCNET', 'SSCNET_E', 'EDGENET_EF', 'EDGENET_D',
                'EDGENET_MF', 'EDGENET_MF_D', 'EDGENET_MF2', 'EDGENET_MF2_D', 'EDGENET_E']

    for net in networks:
        if ((net+'_LR') == (weights[0:len(net)+3])) or ((net+'fine_LR') == (weights[0:len(net)+7])):
            return net

    print("Invalid weight file: ", weights)
    exit(-1)

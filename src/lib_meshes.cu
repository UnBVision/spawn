/*
    Mesh cuda functions
    Adapted to use with Python and numpy

*/

//nvcc --ptxas-options=-v --compiler-options '-fPIC' -o lib_meshes.so --shared lib_meshes.cu
#include <fstream>

int NUM_THREADS=128;
int DEVICE = 0;
float *parameters_GPU;
float sample_neg_obj_ratio=1;
int debug = 0;
int normals_offset = 3;

#define NUM_CLASSES (256)
#define MAX_DOWN_SIZE (1000)

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

/* A utility function to calculate area of triangle formed by (x1, y1),
   (x2, y2) and (x3, y3) */
__device__
float area(float x1, float y1, float x2, float y2, float x3, float y3)
{
   return abs((x1*(y2-y3) + x2*(y3-y1)+ x3*(y1-y2))/2.0);
}

/* A function to check whether point P(x, y) lies inside the triangle formed
   by A(x1, y1), B(x2, y2) and C(x3, y3) */
__device__
unsigned char isInside(float x1, float y1, float x2, float y2, float x3, float y3, float x, float y)
{
   /* Calculate area of triangle ABC */
   float A = area (x1, y1, x2, y2, x3, y3);

   /* Calculate area of triangle PBC */
   float A1 = area (x, y, x2, y2, x3, y3);

   /* Calculate area of triangle PAC */
   float A2 = area (x1, y1, x, y, x3, y3);

   /* Calculate area of triangle PAB */
   float A3 = area (x1, y1, x2, y2, x, y);

   /* Check if sum of A1, A2 and A3 is same as A */
   if (abs(A - (A1 + A2 + A3)) < 0.00005)
      return ((unsigned char) 1);
   else
      return ((unsigned char) 0);
}
__global__
void in_triangles_kernel(float* triangles_GPU, int *num_triangles_GPU, float *points_GPU, int *points_size_GPU, unsigned char *idx_GPU){
  //Z-UP
  //Rerieve pixel coodinates
  int point_idx = threadIdx.x + blockIdx.x * blockDim.x;
  if (point_idx >= *points_size_GPU)
    return;

  int x_idx = point_idx * 2 + 0;
  int y_idx = point_idx * 2 + 1;

  float x = points_GPU[x_idx];
  float y = points_GPU[y_idx];

  //if (threadIdx.x == 0)
  //      printf("kernel %2.2f %2.2f\n",x,y);

  for (int tr_idx=0; tr_idx<*num_triangles_GPU;tr_idx++) {
      int x1_idx = tr_idx * 6 + 0;
      int y1_idx = tr_idx * 6 + 1;
      int x2_idx = tr_idx * 6 + 2;
      int y2_idx = tr_idx * 6 + 3;
      int x3_idx = tr_idx * 6 + 4;
      int y3_idx = tr_idx * 6 + 5;
      float x1 = triangles_GPU[x1_idx];
      float y1 = triangles_GPU[y1_idx];
      float x2 = triangles_GPU[x2_idx];
      float y2 = triangles_GPU[y2_idx];
      float x3 = triangles_GPU[x3_idx];
      float y3 = triangles_GPU[y3_idx];
      //if (threadIdx.x == 0)
      //     printf("%d kernel %2.2f %2.2f %2.2f %2.2f %2.2f %2.2f\n",tr_idx, x1,y1,x2,y2,x3,y3);

      if (isInside(x1, y1, x2, y2, x3, y3, x, y) == 1) {
         //printf("antes %d\n", point_idx);
         idx_GPU[point_idx] = 1;
         //printf("dep %d\n", point_idx);
         return;
      }
  }
  idx_GPU[point_idx] = 0;
}


void in_triangles_CPP(float* triangles, int num_triangles, float *points, int points_size, unsigned char *idx) {

  float *points_GPU;
  int *points_size_GPU;
  float *triangles_GPU;
  int *num_triangles_GPU;
  unsigned char *idx_GPU;

  //printf("nt %d  ps %d\n" , num_triangles, points_size);

  //printf("triangs %2.2f %2.2f %2.2f %2.2f %2.2f %2.2f\n",triangles[0],triangles[1],triangles[2],triangles[3],triangles[4],triangles[5]);

  gpuErrchk(cudaMalloc(&points_GPU, points_size * 2 * sizeof(float)));
  gpuErrchk(cudaMalloc(&points_size_GPU, sizeof(int)));
  gpuErrchk(cudaMalloc(&triangles_GPU, num_triangles * 3 * 2 * sizeof(float)));
  gpuErrchk(cudaMalloc(&num_triangles_GPU, sizeof(int)));
  gpuErrchk(cudaMalloc(&idx_GPU, points_size * sizeof(unsigned char)));

  gpuErrchk(cudaMemcpy(points_GPU, points, points_size * 2 * sizeof(float), cudaMemcpyHostToDevice));
  gpuErrchk(cudaMemcpy(points_size_GPU, &points_size, sizeof(int), cudaMemcpyHostToDevice));
  gpuErrchk(cudaMemcpy(triangles_GPU, triangles, num_triangles * 2 * 3 * sizeof(float), cudaMemcpyHostToDevice));
  gpuErrchk(cudaMemcpy(num_triangles_GPU, &num_triangles, sizeof(int), cudaMemcpyHostToDevice));

  int NUM_BLOCKS = int((points_size + size_t(NUM_THREADS) - 1) / NUM_THREADS);

  in_triangles_kernel<<<NUM_BLOCKS, NUM_THREADS>>>(triangles_GPU, num_triangles_GPU, points_GPU, points_size_GPU, idx_GPU);

  gpuErrchk( cudaPeekAtLastError() );
  gpuErrchk( cudaDeviceSynchronize() );

  cudaMemcpy(idx, idx_GPU,  points_size * sizeof(unsigned char), cudaMemcpyDeviceToHost);

  cudaFree(points_GPU);
  cudaFree(points_size_GPU);
  cudaFree(triangles_GPU);
  cudaFree(num_triangles_GPU);
  cudaFree(idx_GPU);

}


extern "C" {

    void InTriangles(float* triangles,
                      int num_triangles,
                      float *points,
                      int points_size,
                      unsigned char *idx){
                             in_triangles_CPP(triangles,
                                              num_triangles,
                                              points,
                                              points_size,
                                              idx);
                 }
}
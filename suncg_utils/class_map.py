import os

classes_NYU = {
    " " : 0,
    "ceil" : 1,
    "floor" : 2,
    "wall" : 3,
    "window" : 4,
    "chair" : 5,
    "bed" : 6,
    "sofa" : 7,
    "table" : 8,
    "tvs":9,
    "furn":10,
    "objs":11}


def read(python_source='./'):
    with open(os.path.join(os.path.dirname(python_source), "data", "model_class.map")) as f:
        cmaps = f.readlines()
    class_map = {}
    for cmap in cmaps:
        model, class_root = cmap.split()
        class_map.update({model: class_root})
    return class_map

def read_from_matlab(): #(python_source='./'):
    import os
    import scipy.io as spio

    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

    MAT_FILE = os.path.join(ROOT_DIR, '../..', 'matlab/suncgObjcategory.mat')
    #print(MAT_FILE)

    mat = spio.loadmat(MAT_FILE, squeeze_me=True)

    ob = mat["objcategory"]

    cat_names = ob['allcategories'][()]

    object_types = ob['all_labeled_obj'][()]
    class_ids = ob['classid'][()]
    classroot = ob['classRootId'][()]
    classNYU40 = ob['classNYU40'][()]
    classNYU11 = ob['classNYU11'][()]
    classroot_names = ob['categoryRoot'][()]

    classmap = {}
    for i, object_type in enumerate(object_types):
        classmap.update({
            object_type: {
                'classid': class_ids[i],
                'classname': cat_names[class_ids[i] - 1],
                'classroot_id': classroot[i],
                'classroot_name': classroot_names[i],
                'classNYU40': classNYU40[i],
                'classNYU11': classNYU11[i],
                'idNYU11': classes_NYU[classNYU11[i]]
            }
        })
    return classmap


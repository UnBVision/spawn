import numpy as np

def camPose2Extrinsics(cameraPose):
     # cameraPose: & vx, & vy, & vz, & tx, & ty, & tz, & ux, & uy, & uz, & rx, & ry, & rz
     # extrinsics: camera to world
     tv = cameraPose[3:6]
     uv = cameraPose[6:9]
     rv = np.cross(tv, uv)

     return np.column_stack((rv, -uv, tv, cameraPose[0:3]))


def camPose2Extrinsics_zup(cameraPose):
     # cameraPose: & vx, & vy, & vz, & tx, & ty, & tz, & ux, & uy, & uz, & rx, & ry, & rz
     # extrinsics: camera to world
     ext_yup = camPose2Extrinsics(cameraPose)

     #y-up to z-up
     zup = np.array([[1, 0, 0],[0, 0, 1,], [0, 1, 0]])

     ext_zup33 = zup @ ext_yup[0:3,0:3] #@ zup

     t = np.transpose([ext_yup[[0,2,1],3]])

     return np.append(ext_zup33, t, axis=-1 )  #


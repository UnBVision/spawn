import numpy as np
import struct
from lib_utils import debug

def read_string(f):
    c_list = []
    c = f.read(1)
    while c != b'\n':
        c_list.extend([c])
        c = f.read(1)
    out = b''.join(c_list).decode("utf-8")
    return out


def read(filename):
    with open(filename, 'rb') as f:
        # Read header
        header = read_string(f)
        #debug.info(">"+header+"<")

        # Read dim, translate, scale
        width, height, depth = 0, 0, 0
        scale = 1.0
        translate = []

        record = read_string(f).split(sep=' ')
        while record[0] != 'data':
            if record[0] == 'dim':
                width, height, depth = np.array(record[1:], dtype=int)
                #debug.info("%s %s %s" %(width, height, depth) )
            if record[0] == 'translate':
                translate = np.array(record[1:], dtype=float)
                #debug.info(translate)
            if record[0] == 'scale':
                scale = float(record[1])
                #debug.info(scale)
            record = read_string(f).split(sep=' ')

        # Allocate
        scale = scale / width
        totalsize = width * height * depth
        voxels = np.zeros((width, height, depth), np.uint8).flatten()

        # Read voxel data
        index = 0
        end_index = 0
        nr_voxels = 0

        while end_index < totalsize:
            value, count = struct.unpack("2B", f.read(2))
            end_index = index + count

            if end_index >= totalsize:
                break

            voxels[index:end_index] = np.repeat(value, count)

            if value > 0:
                nr_voxels = nr_voxels + count
            index = end_index

        voxels = voxels.reshape((width, height, depth))
        #print("%d voxels retrieved!" % nr_voxels)

    return voxels, scale, translate



def get_NYU_classes():
    return {
    " " : 0,
    "ceil" : 1,
    "floor" : 2,
    "wall" : 3,
    "window" : 4,
    "chair" : 5,
    "bed" : 6,
    "sofa" : 7,
    "table" : 8,
    "tvs":9,
    "furn":10,
    "objs":11}

'''
def read(python_source='./'):
    import os
    with open(os.path.join(os.path.dirname(python_source), "data", "model_class.map")) as f:
        cmaps = f.readlines()
    class_map = {}
    for cmap in cmaps:
        model, class_root = cmap.split()
        class_map.update({model: class_root})
    return class_map
'''

def get_SUNCG_classmap(): #(python_source='./'):
    import os
    import scipy.io as spio

    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

    MAT_FILE = os.path.join(os.path.join(_this_file_path(), 'suncgObjcategory.mat'))
    #print(MAT_FILE)

    mat = spio.loadmat(MAT_FILE, squeeze_me=True)

    ob = mat["objcategory"]

    cat_names = ob['allcategories'][()]

    object_types = ob['all_labeled_obj'][()]
    class_ids = ob['classid'][()]
    classroot = ob['classRootId'][()]
    classNYU40 = ob['classNYU40'][()]
    classNYU11 = ob['classNYU11'][()]
    classroot_names = ob['categoryRoot'][()]

    classmap = {}
    classes_NYU = get_NYU_classes()
    for i, object_type in enumerate(object_types):
        classmap.update({
            object_type: {
                'classid': class_ids[i],
                'classname': cat_names[class_ids[i] - 1],
                'classroot_id': classroot[i],
                'classroot_name': classroot_names[i],
                'classNYU40': classNYU40[i],
                'classNYU11': classNYU11[i],
                'idNYU11': classes_NYU[classNYU11[i]]
            }
        })
    return classmap




def _this_file_path():
    import pathlib
    return pathlib.Path(__file__).parent.absolute()


def get_894_11_class_mapping():
    from scipy.io import loadmat
    import os

    m = loadmat(os.path.join(_this_file_path(), 'NYUClassMapping.mat'))
    classes11 = dict(zip([x[0] for x in m['elevenClass'][0]], range(11)))
    classes40 = dict(zip([x[0] for x in m['nyu40class'][0]], range(40)))
    classes36 = dict(zip([x[0] for x in m['p5d36class'][0]], range(36)))
    m894_40 = [classes40[x[0]] for x in m['mapNYU894To40'][0]]
    m40_36 = [classes36[x[0]] for x in m['mapNYU40to36'][0]]
    m36_11 = [classes11[x[0]] for x in m['map36to11'][0]]

    m894_11 = [m36_11[m40_36[m894_40[x]]] for x in range(894)]

    #print(classes11.keys())
    #map894_11 = [0] + [x + 1 for x in nyu.get_894_11_class_mapping()]

    return [0] + [x + 1 for x in m894_11]

def get_894_40_class_mapping():
    from scipy.io import loadmat
    import os

    m = loadmat(os.path.join(_this_file_path(), 'NYUClassMapping.mat'))
    classes11 = dict(zip([x[0] for x in m['elevenClass'][0]], range(11)))
    classes40 = dict(zip([x[0] for x in m['nyu40class'][0]], range(40)))
    classes36 = dict(zip([x[0] for x in m['p5d36class'][0]], range(36)))
    m894_40 = [classes40[x[0]] for x in m['mapNYU894To40'][0]]
    print(classes40)
    #print(classes11.keys())
    #map894_11 = [0] + [x + 1 for x in nyu.get_894_11_class_mapping()]

    return [0] + [x + 1 for x in m894_40]

def get_nyu40_class_dict():
    from scipy.io import loadmat
    import os

    m = loadmat(os.path.join(_this_file_path(), 'NYUClassMapping.mat'))
    classes40 = dict(zip([x[0] for x in m['nyu40class'][0]], range(40)))

    return classes40


def extract_2d_labels(nyu_path):

    import h5py
    import numpy as np
    import cv2
    import numpy as np

    f = h5py.File('/d02/data/NYU_V2/nyu_depth_v2_labeled.mat', 'r')
    print(f.keys())
    labels = np.array(f['labels'])

    cv2.imwrite("filename.png", np.zeros((10, 10)))

    # dict_keys(['ceiling', 'floor', 'wall', 'window', 'chair', 'bed', 'sofa', 'table', 'tvs', 'furn', 'objs'])

    class_colors = np.flip(np.array([
        [0, 0, 0],
        [0, 1, 0],
        [0, .7, 0],
        [0.66, 0.66, 1],
        [0.8, 0.8, 1],
        [1, 1, 0.0392201],
        [1, 0.490452, 0.0624932],
        [0.657877, 0.0505005, 1],
        [0.0363214, 0.0959549, 0.6],
        [0.316852, 0.548847, 0.186899],
        [8, .5, .1],
        [1, 0.241096, 0.718126],
    ]), 1)


    label_image = np.zeros((480, 640, 3), dtype=np.uint8)
    map894_11 = get_894_11_class_mapping()

    def color_mapper(x):
        class_colors[map894_11[x]] * 255
    v_color_mapper = np.vectorize(color_mapper)

    print(labels.shape)

    for label_data in labels[0:0]:

        import time
        start_time = time.time()

        for x in range(640):
            for y in range(480):
                label_image[y, x] = class_colors[map894_11[label_data[x,y]]] * 255

        print("--- %s seconds ---" % (time.time() - start_time))

        cv2.imwrite("filename.png", label_image)

def get_class_colors():
    import numpy as np
    class_colors = np.array([
        [0, 0, 0], # 0 empty
        [0, 1, 0], # 1 ceil
        [0, .7, 0], # 2 floor
        [0.66, 0.66, 1], # 3 wall
        [0.8, 0.8, 1], # 4 window
        [1, 1, 0.0392201], # 5 chair
        [1, 0.490452, 0.0624932], # 6 bed
        [0.657877, 0.0505005, 1], # 7 sofa
        [0.0363214, 0.0959549, 0.6], # 8 table
        [0.316852, 0.548847, 0.186899], # 9 tvs
        [.8, .5, .1], # 10 furn
        [1, 0.241096, 0.718126], # 11 objs
    ])

    return class_colors


def get_class_colors40():
    import numpy as np
    class_colors = np.array([
        [0.66, 0.66, 1],  # 1 wall
        [0, .7, 0],  # 2 floor
        [.8, .5, .1],  # 3 cabinet
        [1, 0.490452, 0.0624932],  # 4 bed
        [1, 1, 0.0392201],  # 5 chair
        [0.657877, 0.0505005, 1],  # 6 sofa
        [0.0363214, 0.0959549, 0.6],  # 7 table
        [1, 0.241096, 0.718126],  # 8 door
        [0.8, 0.8, 1],  # 9 window
        [.7, .45, .9],  # 10 bookshelf
        [.95, 0.3, 0.65],  # 11 picture
        [.65, .4, .85],  # 12 counter
        [0.7, 0.7, .9],  # 13 blinds
        [0.03, 0.09, 0.55],  # 14 desk
        [.6, .35, .8],  # 15 shelves
        [0.65, 0.65, .85],  # 16 curtain
        [.55, .3, .75],  # 17 dresser
        [.9, 0.45, 0.06],  # 18 pillow
        [.9, 0.25, 0.6],  # 19 mirror
        [.1, .6, .1],  # 20 floor mat
        [.8, 0.25, 0.55],  # 21 clothes
        [0, 1, 0], # 22 ceiling
        [.75, 0.25, 0.50],  # 23 books
        [.7, 0.2, 0.45],  # 24 refridgerator
        [0.316852, 0.548847, 0.186899],  # 25 tvs
        [.65, 0.1, 0.35],  # 26 paper
        [.6, 0.1, 0.3],  # 27 towel
        [0.6, 0.6, .8],  # 28 shower curtain
        [.55, 0.1, 0.25],  # 29 box
        [.5, 0.05, 0.2],  # 30 whiteboard
        [.5, .5, .5],  # 31 person
        [.5, .25, .7],  # 32 night stand
        [.7, .7, .7],  # 33 toilet
        [.6, .6, .6],  # 34 sink
        [.9, .9, .8],  # 35 lamp
        [.7, .7, .7],  # 36 bathtub
        [.55, 0.1, 0.25],  # 37 bag
        [0, .6, 0], # 38 other structure
        [.45, .2, .65],  # 39 otherfurniture
        [.5, 0.05, 0.2],  # 40 otherprop
        [0, 0, 0],  # 0 empty

    ])
    #     {'wall': 0, 'floor': 1, 'cabinet': 2, 'bed': 3, 'chair': 4, 'sofa': 5, 'table': 6, 'door': 7, 'window': 8,
    #     'bookshelf': 9, 'picture': 10, 'counter': 11, 'blinds': 12, 'desk': 13, 'shelves': 14, 'curtain': 15,
    #     'dresser': 16, 'pillow': 17, 'mirror': 18, 'floor mat': 19, 'clothes': 20, 'ceiling': 21, 'books': 22,
    #     'refridgerator': 23, 'television': 24, 'paper': 25, 'towel': 26, 'shower curtain': 27, 'box': 28, 'whiteboard': 29,
    #     'person': 30, 'night stand': 31, 'toilet': 32, 'sink': 33, 'lamp': 34, 'bathtub': 35, 'bag': 36,
    #     'otherstructure': 37, 'otherfurniture': 38, 'otherprop': 39}

    return np.array(class_colors)


def get_class_colors_bgr():
    import numpy as np
    return np.flip(get_class_colors(), 1)

def get_class_colors_bgr40():
    import numpy as np
    return np.flip(get_class_colors40(), 1)


def get_suncg_categories_file(suncgToolboxPath):
    import os
    return os.path.join(suncgToolboxPath,"metadata","ModelCategoryMapping.csv")

def get_suncg_idx2nyu40_map(suncgToolboxPath):
    import pandas as pd
    import numpy as np
    nyu40_class_dict = get_nyu40_class_dict()

    suncg_categories_file = get_suncg_categories_file(suncgToolboxPath)

    suncg_cat_df = pd.read_csv(suncg_categories_file)

    suncg_idx2nyu40 = np.zeros((max(suncg_cat_df.index + 1)),np.int)

    for idx in range(max(suncg_cat_df.index + 1)):
        classNYU40 = suncg_cat_df.iloc[idx].nyuv2_40class.replace("_"," ")

        if classNYU40=="void":
            suncg_idx2nyu40[idx] = 0
        else:

            try:
                suncg_idx2nyu40[idx]=nyu40_class_dict[classNYU40] + 1
                #print(idx, suncg_cat_df.iloc[idx].model_id, suncg_cat_df.iloc[idx].fine_grained_class, classNYU40)
            except:
                print("Class not found on idx:", idx)
                print("model_id", suncg_cat_df.iloc[idx].model_id)
                print("fine_grained_class", suncg_cat_df.iloc[idx].fine_grained_class)
                print("classNYU40", classNYU40)
                exit(-1)

    return suncg_idx2nyu40

def get_suncg_idx2nyu11_map(suncgToolboxPath):
    import pandas as pd
    import numpy as np
    suncg_cm = get_SUNCG_classmap()
    suncg_categories_file = get_suncg_categories_file(suncgToolboxPath)

    suncg_cat_df = pd.read_csv(suncg_categories_file)

    suncg_idx2nyu11 = np.zeros((max(suncg_cat_df.index + 1)),np.int)

    suncg2nyu = {'Wall':'wall', 'Ceiling':'ceil','Floor':'floor', 'Box':'window'}

    for idx in range(max(suncg_cat_df.index + 1)):
        classes_nyu = get_NYU_classes()
        if idx == 0:
             suncg_idx2nyu11[idx]=idx
        elif idx <= 4:
            suncg_idx2nyu11[idx]=classes_nyu[suncg2nyu[suncg_cat_df.iloc[idx].model_id]]
        else:
            #print(suncg_cm[suncg_cat_df.iloc[idx].model_id])
            suncg_idx2nyu11[idx]=suncg_cm[suncg_cat_df.iloc[idx].model_id]['idNYU11']

    return suncg_idx2nyu11

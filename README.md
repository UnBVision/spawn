# SPAwN

This is the source code for reproducing results of paper **Data Augmented 3D Semantic Scene Completion with 2D Segmentation Priors (WACV 2022)**, available from https://openaccess.thecvf.com/content/WACV2022/html/Dourado_Data_Augmented_3D_Semantic_Scene_Completion_With_2D_Segmentation_Priors_WACV_2022_paper.html. 

We kindly request that the above paper be cited if the code (or parts of it) shared here is used, for any purpose.

## Setup

### Hardware Requirements
To run this project you will need a NVIDIA compatible gpu with at least 10GB memory.

### Software Requirements

We use CUDA and Pytorch in this project. We also use Anaconda to manage our software environment.
Due to cv2 incompatibilities with pytorch we suggest to use two distinct environments for Dataset preparation and Training

Step-by-step configuration of our environment:

#### Environment for Datasets Preparation
``` shell
    conda create -n cv2 python=3.8
    conda activate cv2
    conda install -c menpo opencv
    conda install -c anaconda scikit-image 
    conda install -c conda-forge tqdm
    conda install -c conda-forge numpy
```

#### Environment for Training and Evaluating

``` shell
    conda create -n torch python=3.8
    conda activate torch
    conda install -c anaconda jupyter
    conda install pytorch torchvision torchaudio cudatoolkit=11.0 -c pytorch
    conda install -c anaconda scikit-image 
    conda install -c conda-forge tqdm
    conda install -c conda-forge tensorboard
    conda install -c anaconda pandas
```

## Datasets Download and organization

### SSCNet Scenes, camera positions, evaluation volumes

We use the same scenes used in the seminal SSCNet paper for training and evaluating. 
Because of that, you are required to download those information from https://github.com/shurans/sscnet (download_data.sh)


### SUNCG Dataset

SUNCG is a very large dataset. We use SUNCG as a pretraining dataset to enhance our models. 
This step is not strictly required.
You can train our models directly on NYUDv2 or NYUCAD without pretraining.   

If you want to use SUNCG, you are required to download SUNCG from https://sscnet.cs.princeton.edu/ 

Our SUNCG images are rendered directly from the SUNCG raw models. You also need to download SUNCG Toolbox: https://github.com/tinytangent/SUNCGtoolbox

### NYUDv2 Dataset

Download NYUDv2 from http://horatio.cs.nyu.edu/mit/silberman/nyu_depth_v2/nyu_depth_v2_labeled.mat for 2D labels and RGB images

### NYUCAD

NYUCAD depth map and 3D Ground truth and is in https://github.com/shurans/sscnet. RGB images are the same of NYUDv2. 

### Datasets Organization 

The data folders should be organized as follows:
``` shell
    suncg                     //SunCG RAW Dataset Root (see  http://sscnet.cs.princeton.edu/)
         |-- house                 //JSON scene files
         |-- object                //object models (Wavefront OBJ and MTL files) 
         |-- object_vox            //object occupancy
         |-- room                  //room structural objects (Floor, Ceiling, Walls)
         |-- texture               //object textures  

    nyu                      //NYU depth files and binary files with 3D ground truth
         |-- NYUtrain 
                        |-- xxxxx_0000.png //depth Rename to xxxxx_0000_depth.png
                        |-- xxxxx_0000.bin //3D Ground Truth
         |-- NYUtest
                        |-- xxxxx_0000.png //depth Rename to xxxxx_0000_depth.png
                        |-- xxxxx_0000.bin
         |-- nyu_depth_v2_labeled.mat  //matlab data file containing NYU-D-V2 dataset 

    nyucad
         |-- NYUtrain 
                        |-- xxxxx_0000.png
                        |-- xxxxx_0000.bin 
         |-- NYUtest
                        |-- xxxxx_0000.png
                        |-- xxxxx_0000.bin
    

```
## Datasets preparation

### Cuda code for dataset preparation
First of all, compile the CUDA code for data preparation:
``` shell
    cd src
    nvcc --ptxas-options=-v --compiler-options '-fPIC' -o lib_edgenetv2.so --shared lib_edgenetv2.cu
```
### Activate the proper environment
``` shell
    conda activate cv2
```



### Paths configuration
We provide an example path.conf file. Adjust the paths accordingly.

### SUNCG

To render updated RGB and depth images from raw SUNCG models:

``` shell
    python suncg_rgb_extract.py
```
To extract SUNCG GT from raw SUNCG models:

``` shell
    python suncg_rgb_extract.py
```

### NYU 

To extract 2D labels (11 classes) from the downloaded Matlab file (.mat):
``` shell
    python extract_nyu_2d_labels.py
```

### All datasets
To compute Surface Normals (check help --help):
``` shell
    python get_normals.py
```
For NYUCAD, this also render a 2D GT from the 3D Ground Truth to reduce misalignment 

## 2D Networks training (jupyter notebooks)

First activate the proper environment
``` shell
    conda activate torch
```


Single mode: refinenet_train.ipynb

Bimodal: bimodal_train.ipynb

## 3D Preprocessing
We preprocess the 3D datasets off-line to accelerate training. 
Preprocessing includes: F-TSDF computation for depth and 3D priors projection.

First of all, compile the CUDA preprocessing library:
``` shell
    cd src
    nvcc --ptxas-options=-v --compiler-options '-fPIC' -o lib_preproc.so --shared lib_preproc.cu
```

Then, preprocess the desired datasets (check help --help for more options):
``` shell
    python spawn_preproc.py [dataset] [2D weigths file]
```

## 3D Networks Training
Training script uses preprocessed datasets. Preprocess before training.
To train any 3D network (check help --help for more options, including how to train baseline SSCNET):
``` shell
    python spawn_train.py NYU --alpha=0 --data_aug=yes
```
## 3D Evaluation
In folder "weights" we provide pretrained models for:
- SUNCG(no data augmentation):  R329_MMNet_SUPERV-SUNCG-depth+rgb+normals (MIoU 78.9%)
- NYUDv2(pretrained on SUNCG with data augmentation): R333_MMNet_SUPERV-NYU-depth+rgb+normals_da (MIoU 46.8%)
- NYUCAD(pretrained on SUNCG with data augmentation): R339_MMNet_SUPERV-NYUCAD-depth+rgb+normals_da (MIoU 65.3%)

To get full evaluation results, as presented in our paper(check help --help for more options):
``` shell
    python spawn_eval.py NYUCAD R339_MMNet_SUPERV-NYUCAD-depth+rgb+normals_da
```
The model you want to evaluate is a parameter passed to the evaluation script. 

To perform Test-Time Data Augmentation evaluation:
``` shell
    python spawn_eval_ttda.py NYUCAD R339_MMNet_SUPERV-NYUCAD-depth+rgb+normals_da
```


## 3D Scene Rendering

We provide a WaveFront OBJ generator to reproduce the images presented in the qualitative analysis section of our paper.
Given a scene, a dataset and a model, the provided tool generates .obj and .mtl files for:
- visible surface;
- projected semantic priors;
- ssc prediction;
- GT.

We also provide some Blender (open-source 3D software) templates with light and ambient configurations for high-quality scene rendering.

To generate .obj and .mtl files (--help for help):
``` shell
    python spawn2obj.py
```

Example .obj and .mtl files and Blender templates are provided in the obj folder. 

##
